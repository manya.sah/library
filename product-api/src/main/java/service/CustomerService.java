package service;

import entity.CustomerEntity;
import entity.Priscription;

import java.util.List;

public interface CustomerService {
    public Priscription checkPriscrisption(long id);
    public List<Priscription> getPriscriptionByCustomerId(long id);
    public CustomerEntity getCustomerById(long id);
}
