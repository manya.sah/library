package service;

import entity.CustomerEntity;
import entity.Priscription;
import repository.CustomerRepository;

import java.util.List;
import java.util.Optional;

public class CustomerServiceImpl implements CustomerService{
    private final CustomerRepository repository;

    public CustomerServiceImpl(CustomerRepository repository) {
        this.repository = repository;
    }
    @Override
    public Priscription checkPriscrisption(long id) {
        CustomerEntity customerEntity = repository.getByCustomerId(id);
        List<Priscription> priscriptionList = customerEntity.getPriscriptionList();

        return null;
    }

    @Override
    public List<Priscription> getPriscriptionByCustomerId(long id) {
        CustomerEntity customerEntity=repository.getByCustomerId(id);
        List<Priscription> priscriptionList=customerEntity.getPriscriptionList();
        return priscriptionList;
    }

    @Override
    public CustomerEntity getCustomerById(long id) {
        return repository.getByCustomerId(id);
    }
}
