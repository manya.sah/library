package response;

import entity.CustomerEntity;
import entity.Priscription;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class CustomerResponse {
    public CustomerEntity customer;
    public List<Priscription> priscriptionList;
}
