package controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import entity.CustomerEntity;
import entity.PackageEntity;
import entity.Priscription;
import entity.ProductEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import response.CustomerResponse;
import service.CustomerService;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class CustomerRestController {
    RestTemplate restTemplate;
    @Autowired
    private CustomerService customerService;

    @GetMapping("/{customer}")
    @ModelAttribute("customer")
    public CustomerEntity getCustomerById(@PathVariable long id){
        CustomerEntity customer=restTemplate.getForObject("some/mock/url"+id,CustomerEntity.class);
        return customer;
    }
    public CustomerResponse getByExchangeFallback(){
        CustomerResponse customerResponse=new CustomerResponse();
        customerResponse.getCustomer().setCustomerID(-1);
        return customerResponse;
    }

    @GetMapping("/{restCustomer}")
    @ModelAttribute("customer")
    @HystrixCommand(fallbackMethod = "getByExchangeFallback",commandKey="exchangeFallBack",ignoreExceptions = Throwable.class)
    public CustomerResponse getByExchange() {
        HttpEntity request = new HttpEntity<>(CustomerService.class);
        ResponseEntity<CustomerResponse> response = restTemplate.exchange("some/mock/url", HttpMethod.POST, request, CustomerResponse.class);
        CustomerResponse customer=response.getBody();
        return customer;
    }
    @GetMapping("/{callPackageAPI}")

    public List<ProductEntity> packageResponse(@RequestParam long custId, @RequestParam long productId) throws Exception {
        List<Priscription> priscriptionList=customerService.getPriscriptionByCustomerId(custId);
        List<ProductEntity> productResponse=new ArrayList<ProductEntity>();


        HttpEntity request= new HttpEntity<>(productId);
        ProductEntity product=restTemplate.exchange("package/api/url/getProduct",HttpMethod.GET,request,ProductEntity.class).getBody();


        for (Priscription priscription:priscriptionList){
            List<PackageEntity> packageEntities=product.getPackageList();
            for(PackageEntity packageEntity:packageEntities){
                if(priscription.getLeftPow()==packageEntity.getLeftPower() && priscription.getRightPow()==packageEntity.getRightPower()){
                    productResponse.add(product);
                }
            }
        }
        if(productResponse.size()>0){
            return productResponse;
        }
        else {
            throw new Exception();
        }

    }
}
