package controller;

import entity.CustomerEntity;
import entity.Priscription;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import response.CustomerResponse;
import service.CustomerService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class CustomerController {
    @Autowired
    private CustomerService service;
    @ModelAttribute("customerResponse")
    @GetMapping("getpriscriptionbycustomerid/{customerId}")
    public CustomerResponse getPriscription(@PathVariable long customerId)

    {
        CustomerEntity cusotmer=new CustomerEntity();
        List<Priscription> priscriptionList=new ArrayList<Priscription>();
        try {
            cusotmer=service.getCustomerById(customerId);
            if(cusotmer.getCustomerID()==0){
                throw new Exception();
            }
            priscriptionList=service.getPriscriptionByCustomerId(customerId);
            if(priscriptionList.size()==0){
                throw new Exception();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        CustomerResponse customerResponse=new CustomerResponse(cusotmer,priscriptionList);
        return customerResponse;
    }


}
