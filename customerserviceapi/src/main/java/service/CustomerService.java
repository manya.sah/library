package service;

import model.CustomerEntity;
import model.CustomerResponse;
import model.ProductEntity;
import model.ProductResponse;

public interface CustomerService {
    public CustomerEntity getCustomerById(long id);
    public ProductEntity getProductById(long id);
}
