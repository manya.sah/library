package service;

import model.CustomerEntity;
import model.CustomerResponse;
import model.ProductEntity;
import model.ProductResponse;
import repository.CustomerRepository;
import repository.ProductRepository;

public class CustomerServiceImpl implements CustomerService{
    CustomerRepository customerRepository;
    ProductRepository productRepository;
    CustomerServiceImpl(CustomerRepository customerRepository,ProductRepository productRepository){
        this.customerRepository=customerRepository;
        this.productRepository=productRepository;

    }

    @Override
    public CustomerEntity getCustomerById(long id) {
       return customerRepository.getByCustomerId(id);
    }

    @Override
    public ProductEntity getProductById(long id) {
      return   productRepository.findByProductId(id);
    }
}
