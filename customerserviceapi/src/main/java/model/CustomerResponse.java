package model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatusCode;

@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CustomerResponse  extends ResponseEntity {
    public CustomerEntity customer;


}
