package controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import model.CustomerEntity;
import model.CustomerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import service.CustomerService;

@AllArgsConstructor
@RequiredArgsConstructor
@RestController
@Getter
@Setter
public class CustomerRestController {
    RestTemplate restTemplate;
    @Autowired
    private CustomerService customerService;

    @GetMapping("/{customer}")
    @ModelAttribute("customer")
     public CustomerEntity getCustomerById(@PathVariable long id){
        CustomerEntity customer=restTemplate.getForObject("some/mock/url"+id,CustomerEntity.class);
        return customer;
    }
    @GetMapping("/{restCustomer}")
    @ModelAttribute("customer")
    public CustomerResponse getByExchange() {
        HttpEntity request = new HttpEntity<>(CustomerService.class);
        ResponseEntity<CustomerResponse> response = restTemplate.exchange("some/mock/url", HttpMethod.POST, request, CustomerResponse.class);
        CustomerResponse customer=response.getBody();
        return customer;
    }
}
