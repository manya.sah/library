package controller;
import model.CustomerResponse;
import model.ProductResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import repository.CustomerRepository;
import service.CustomerService;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {
    @Autowired
    private CustomerService service;
    @GetMapping("/{customer}")
    @ModelAttribute("response")
    public Map<String,Object> response(@PathVariable long custId, @RequestParam long productId){
        Map<String,Object> response=new HashMap<String,Object>();
        CustomerResponse customerResponse=new CustomerResponse();
        customerResponse.setCustomer(service.getCustomerById(custId));
        response.put("customerResponse",customerResponse);
        ProductResponse productResponse=new ProductResponse(service.getProductById(productId));
        response.put("productResponse",productResponse);
        return response;
    }
    //how to add multiple objects in a single response // RESOLVED
    // i thought of using a map of<String,Object> type and addingi it to model attribute.
    //is there any other way
}
