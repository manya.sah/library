package repository;




import model.ProductEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.LongSummaryStatistics;

public interface ProductRepository extends MongoRepository<ProductEntity, Long> {
    public ProductEntity findByProductId( long id);

}