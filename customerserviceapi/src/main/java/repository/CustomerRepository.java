package repository;


import model.CustomerEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<CustomerEntity, Long> {
    public CustomerEntity getByCustomerId(long id);
}




