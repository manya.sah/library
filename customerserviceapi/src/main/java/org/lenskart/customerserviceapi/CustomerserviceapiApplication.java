package org.lenskart.customerserviceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerserviceapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerserviceapiApplication.class, args);
	}

}
