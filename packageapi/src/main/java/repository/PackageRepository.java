package repository;

import entity.PackageEntity;
import entity.ProductEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PackageRepository extends MongoRepository<PackageEntity, Long> {
    public PackageEntity findByProductId (long id);
}
