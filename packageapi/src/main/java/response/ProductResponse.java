package response;

import entity.PackageEntity;
import entity.ProductEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;
@AllArgsConstructor
@RequiredArgsConstructor
@Setter
@Getter
public class ProductResponse {
    public ProductEntity product;
    public List<PackageEntity> packageList;
}
