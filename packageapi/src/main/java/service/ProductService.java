package service;

import entity.PackageEntity;
import entity.ProductEntity;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    public Optional<ProductEntity> findProductById(long id) throws Exception;
    public List<ProductEntity> getAllProduct();
    public ProductEntity addNewProduct(ProductEntity newProduct);
    public ProductEntity replaceProduct(ProductEntity ProductEntity, Long id) throws Exception;
    public ProductEntity getProductByPackage( long Id);
    public List<PackageEntity> displayPackageWithProductId(long id);

}
