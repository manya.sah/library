package service;

import entity.PackageEntity;
import entity.ProductEntity;
import repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductServiceImpl implements ProductService {

    private final ProductRepository repository;

    public ProductServiceImpl(ProductRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<ProductEntity> findProductById(long id) throws Exception {
        Optional<ProductEntity> ProductEntity = repository.findById(id);
        if (!ProductEntity.isPresent()) {
            throw new Exception("Product not found");
        }
        return ProductEntity;
    }


    @Override
    public List<ProductEntity> getAllProduct() {
        return repository.findAll();
    }

    @Override
    public ProductEntity addNewProduct(ProductEntity newProduct) {
        return repository.save(newProduct);
    }

    @Override
    public ProductEntity replaceProduct(ProductEntity ProductEntity, Long id) throws Exception {
        Optional<ProductEntity> optionalProduct = repository.findById(id);
        if (!optionalProduct.isPresent()) {
            throw new Exception("Product not found");
        }
        ProductEntity Product = optionalProduct.get();
        Product.setProductId(ProductEntity.getProductId());
        Product.setName(ProductEntity.getName());
        Product.setCategory(ProductEntity.getCategory());
        Product.setPrice(ProductEntity.getPrice());
        return repository.save(Product);
    }

    @Override
    public ProductEntity getProductByPackage(long Id) {
        return repository.findByPackageId(Id);

    }

    @Override
    public List<PackageEntity> displayPackageWithProductId(long id) {
        ProductEntity productEntity=repository.findById(id).orElse(new ProductEntity());
        List<PackageEntity> packageEntities=new ArrayList<PackageEntity>();
        if(productEntity.getProductId()!=0){
             packageEntities=productEntity.getPackageList();
            if(packageEntities.size()==0){
                System.out.println("No package present  in the  product");
            }


        }
        return packageEntities;
    }
}
