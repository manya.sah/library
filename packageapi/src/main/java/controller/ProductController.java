package controller;

import entity.PackageEntity;
import entity.ProductEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import response.ProductResponse;
import service.ProductService;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class ProductController {
    @Autowired
    private ProductService service;


    @GetMapping("/Product")
    List<ProductEntity> all() {
        return service.getAllProduct();
    }

    @PostMapping("/Product")
    ProductEntity newProduct(@RequestBody ProductEntity newProduct) {
        return service.addNewProduct(newProduct);
    }
    @PutMapping("/Product/{id}")
    ProductEntity replaceProduct(@RequestBody ProductEntity ProductEntity, @PathVariable Long id) throws Exception {

        return  service.replaceProduct(ProductEntity,id);
    }
    @GetMapping("/findProduct/{id}")
    Optional<ProductEntity> findById(@PathVariable Long id) throws Exception {
        if (service.findProductById(id)==null) {
            System.out.println("No Product Found");

        }

        return service.findProductById(id);

    }
    @GetMapping ("/findProductByPackage/{packageid}")
    ProductEntity getProductByPackage(@PathVariable Long packageid){
        return service.getProductByPackage(packageid);
    }
    @ModelAttribute("productResponse")
    @GetMapping ("/findPackagetByProductId/{productid}")
     ProductResponse getPackagesByProduct(@PathVariable Long productid){
        ProductEntity product=new ProductEntity();
        try {
            product=service.findProductById(productid).orElse(new ProductEntity());
            if(product.getProductId()==0){
                throw  new Exception() ;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        List<PackageEntity> packageList=service.displayPackageWithProductId(productid);
       ProductResponse productResponse=new ProductResponse(product,packageList);
       return productResponse;
    }
}