package org.lenskart.packageapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PackageapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PackageapiApplication.class, args);
	}

}
