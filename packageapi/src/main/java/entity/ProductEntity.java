package entity;



import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.List;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document( collection= "ProductTable")
public class ProductEntity {

    @Id
    private long productId;
    @DocumentReference
    List<PackageEntity> packageList;
    private String name;
    private String category ;
    private String  price;


}
