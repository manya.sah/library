package entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.LongSummaryStatistics;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
@Document ( collection= "PackageTable")

public class PackageEntity {
    @DocumentReference
    long productId ;
    String productName;
    String packageName;
    int leftPower;
    int rightPower;
    boolean isPositive;
    boolean isNegative;


}
